"""PyAudio example: Record a few seconds of audio and save to a WAVE file."""

import argparse
import pyaudio
import wave

CHUNK = 1024
FORMAT = pyaudio.paInt16
# RECORD_SECONDS = 5

parser = argparse.ArgumentParser()
parser.add_argument("outputFileName", type=str,
                    help="Name of output WAV file")
parser.add_argument("-s", "--sampleRate", type=int, default=8000,
                    help="Sample rate (default = 8000 sps)")
parser.add_argument("-n", "--numChannels", type=int, default=1,
                    help="Number of channels (default = 1)")
parser.add_argument("-d", "--duration", type=int, default=5,
                    help="Duration (default = 5 sec)")

args = parser.parse_args()
outputFileName = args.outputFileName
sampleRate = args.sampleRate
numChannels = args.numChannels
duration = args.duration

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=numChannels,
                rate=sampleRate,
                input=True,
                frames_per_buffer=CHUNK)

print("* recording")

frames = []

for i in range(0, int(sampleRate / CHUNK * duration)):
    data = stream.read(CHUNK)
    frames.append(data)

print("* done recording")

stream.stop_stream()
stream.close()
p.terminate()

wf = wave.open(outputFileName, 'wb')
wf.setnchannels(numChannels)
wf.setsampwidth(p.get_sample_size(FORMAT))
wf.setframerate(sampleRate)
wf.writeframes(b''.join(frames))
wf.close()
