"""PyAudio Example: Play a WAVE file."""

import pylab as pl
import pyaudio
import wave
import sys

import effects

CHUNK = 1024

if len(sys.argv) < 2:
    print("Plays a wave file.\n\nUsage: %s filename.wav" % sys.argv[0])
    sys.exit(-1)

wf = wave.open(sys.argv[1], 'rb')
sampleRate = wf.getframerate()
numSamples = wf.getnframes()
numChunks = int(numSamples / CHUNK)
data = bytes()

p = pyaudio.PyAudio()

stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                channels=wf.getnchannels(),
                rate=wf.getframerate(),
                output=True)

for i in range(numChunks):
    data += wf.readframes(CHUNK)

numSamples = int(len(data) / 2)

sampleData = pl.frombuffer(data,dtype='int16')

effectSampleData = effects.scramble(sampleData)

effectData = effectSampleData.tobytes()

for i in range(int(2*numChunks)):
    stream.write(effectData[i * CHUNK:(i + 1) * CHUNK])

stream.stop_stream()
stream.close()

p.terminate()

pl.subplot(311)
sampleOffset = \
    pl.where(pl.absolute(sampleData) == max(pl.absolute(sampleData)))[0][0]
sampleOffset = max(sampleOffset - 100, 0)
pl.stem(sampleData[sampleOffset+pl.arange(100)])
pl.stem(effectSampleData[sampleOffset+pl.arange(100)],markerfmt='ro')
pl.title('Original and Scrambled Samples')

pl.subplot(312)
pl.specgram(sampleData,int(.04*sampleRate),sampleRate,
            window=pl.hamming(int(.04*sampleRate)),
            noverlap=int(.02*sampleRate),pad_to=2048)
pl.ylabel('Frequency (Hz)')
pl.title('Original')
pl.subplot(313)
pl.specgram(effectSampleData,int(.04*sampleRate),sampleRate,
            window=pl.hamming(int(.04*sampleRate)),
            noverlap=int(.02*sampleRate),pad_to=2048)
pl.xlabel('Time (seconds)')
pl.ylabel('Frequency (Hz)')
pl.title('Frequency Inverted')
pl.show()
