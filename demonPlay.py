"""PyAudio Example: Play a WAVE file."""

import argparse
import pylab as pl
import pyaudio
import wave
import sys

import effects

parser = argparse.ArgumentParser()
parser.add_argument("inputFileName", type=str,
                    help="Name of input WAV file")
parser.add_argument("-s", "--shift", type=float, default=200.,
                    help="Frequency shift in Hz (default=200 Hz)")

args = parser.parse_args()
inputFileName = args.inputFileName
shift = args.shift

CHUNK = 1024

if len(sys.argv) < 2:
    print("Plays a wave file.\n\nUsage: %s filename.wav" % sys.argv[0])
    sys.exit(-1)

wf = wave.open(inputFileName, 'rb')
sampleRate = wf.getframerate()
numSamples = wf.getnframes()
numChunks = int(numSamples / CHUNK)
data = bytes()

p = pyaudio.PyAudio()

stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                channels=2,
                rate=wf.getframerate(),
                output=True)

for i in range(numChunks):
    data += wf.readframes(CHUNK)

numSamples = int(len(data) / 2)

sampleData = pl.frombuffer(data,dtype='int16')

effectSampleData = effects.magfield(sampleData,sampleRate,shift)
effectSampleData *= 30000 / max(pl.absolute(effectSampleData))
effectSampleData = effectSampleData.astype(pl.int16)

demonSampleData = pl.zeros(2*len(sampleData),dtype='int16')
demonSampleData[::2] = sampleData
demonSampleData[1::2] = effectSampleData

demonData = demonSampleData.tobytes()

for i in range(int(4*numChunks)):
    stream.write(demonData[i * CHUNK:(i + 1) * CHUNK])

stream.stop_stream()
stream.close()

p.terminate()
