import numpy as np
import wave

def audioread(filename, samples=None):
    waveFile = wave.open(filename, 'rb')
    sampleRate = waveFile.getframerate()
    numSamples = waveFile.getnframes()

    if samples is not None:
        numSamples = min(samples, numSamples)

    numChannels = waveFile.getnchannels()
    sampleData = np.frombuffer(waveFile.readframes(numSamples), dtype='int16')

    if numChannels > 1:
        sampleData = np.reshape(sampleData,(numChannels,-1))

    return sampleData, sampleRate
