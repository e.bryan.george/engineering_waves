import numpy as np

def scramble(x: np.ndarray) -> np.ndarray:
    """Scramble samples to disguise audio

    :param x: Input sample value array
    :return: Scrambled sample value array
    """

    # Copy the input array to the output.

    y = x.copy()

    # Change the sign of every other sample.

    y[::2] = -y[::2]

    # Return the output array

    return y

def modulate(x: np.ndarray, fs: int, shift: float):
    freqOffset = 2 * np.pi * shift / fs
    y = np.cos(freqOffset * np.arange(len(x))) * x

    return y

from scipy.signal import hilbert

def magfield(x: np.ndarray, fs: int, shift: float):

    hilbertX = hilbert(x)

    freqOffset = 2 * np.pi * shift / fs

    y = np.real(np.exp(1j*freqOffset*np.arange(len(hilbertX))) * hilbertX)

    return y
